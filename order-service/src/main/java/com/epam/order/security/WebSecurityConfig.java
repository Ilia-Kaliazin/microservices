package com.epam.order.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public TokenAuthenticationFilter jwtAuthenticationTokenFilter() throws Exception {
        return new TokenAuthenticationFilter();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                /*.exceptionHandling().authenticationEntryPoint( restAuthenticationEntryPoint ).and()*/
                /*.sessionManagement().sessionCreationPolicy( SessionCreationPolicy.STATELESS ).and()*/
                .addFilterBefore(jwtAuthenticationTokenFilter(), BasicAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers("/order/**").permitAll()

                .anyRequest().authenticated()
                /*.anyRequest().hasRole("admin") << Works with ROLE entities while we have SimpleGrantedAuthority...*/
                .anyRequest().hasAuthority("admin")

                /*.httpBasic().disable();*/

                /*From https://github.com/bfwg/springboot-jwt-starter*/
                .and().csrf().disable();
    }
}
