package com.epam.order.controller;

import com.epam.order.OrderService;
import com.epam.order.models.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @Autowired
    Validator orderValidator;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(orderValidator);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Order> index() {
        return orderService.getOrders();
    }

    @PostMapping
    public Order create(@RequestBody @Valid Order order) {
        if (order.getItems() != null) order.getItems().forEach(item -> item.setOrder(order));
        return orderService.saveOrder(order);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Order view(@PathVariable("id") long id) {
        return orderService.getOrder(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Order edit(@PathVariable("id") long id, @RequestBody @Valid Order order) {
        if (orderService.getOrder(id) == null) return null;
        else return orderService.saveOrder(order);
    }
}
