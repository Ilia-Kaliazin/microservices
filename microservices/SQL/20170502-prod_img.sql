CREATE TABLE `order_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int not NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

ALTER TABLE product_images add FOREIGN KEY (`order_id`) REFERENCES `products` (`id`);