package com.epam.mentoring.controllers;

import com.epam.mentoring.security.TokenBasedAuthentication;
import com.epam.mentoring.security.TokenHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/auth")
public class SecurityController {

    @Value("${jwt.header}")
    private String AUTH_HEADER;

    @Autowired
    TokenHelper tokenHelper;

    @Autowired
    UserDetailsService userDetailServiceImpl;

    private String getToken(HttpServletRequest request) {

        String authHeader = request.getHeader(AUTH_HEADER);
        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            return authHeader.substring(7);
        }

        return null;
    }

    @RequestMapping(method = RequestMethod.GET)
    public TokenBasedAuthentication getSecurityToken(HttpServletRequest request) {
        String authToken = getToken(request);

        if (authToken != null) {
            // Get username from token
            String userEmail = tokenHelper.getUsernameFromToken(authToken);

            if (userEmail != null) {
                // Create authentication
                return new TokenBasedAuthentication(userDetailServiceImpl.loadUserByUsername(userEmail));
            }
        }
        return null;
    }
}
