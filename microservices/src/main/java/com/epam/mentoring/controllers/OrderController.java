package com.epam.mentoring.controllers;

import com.epam.mentoring.models.Order;
import com.epam.mentoring.security.TokenHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Value("${jwt.header}")
    private String AUTH_HEADER;

    @Value("${url.order-service}")
    private String SERVICE_URL;

    @Autowired
    Validator orderValidator;

    @Autowired
    TokenHelper tokenHelper;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(orderValidator);
    }

    private HttpHeaders createAuthHeaders(HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(AUTH_HEADER, "Bearer " + tokenHelper.generateToken(request.getHeader(AUTH_HEADER)));

        return headers;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Order> index(HttpServletRequest request) {
        return Arrays.asList(
                new RestTemplate().exchange(
                        SERVICE_URL,
                        HttpMethod.GET,
                        new HttpEntity<>(createAuthHeaders(request)),
                        Order[].class).getBody());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Order view(HttpServletRequest request, @PathVariable("id") long id) {
        return new RestTemplate().exchange(
                String.format("%s/%s", SERVICE_URL, id),
                HttpMethod.GET,
                new HttpEntity<>(createAuthHeaders(request)),
                Order.class).getBody();
    }

    @PostMapping
    public Order create(HttpServletRequest request, @RequestBody @Valid Order order) {
        // Required by Hibernate ORM to save properly
        if (order.getItems() != null) order.getItems().forEach(item -> item.setOrder(order));

        return new RestTemplate().postForObject(
                SERVICE_URL,
                new HttpEntity<>(order, createAuthHeaders(request)),
                Order.class);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public Order edit(HttpServletRequest request, @PathVariable("id") long id, @RequestBody @Valid Order order) {
        if (new RestTemplate().getForObject(SERVICE_URL + "/" + id, Order.class) != null) {
            new RestTemplate().exchange(
                    String.format("%s/%s", SERVICE_URL, id),
                    HttpMethod.PUT,
                    new HttpEntity<>(order, createAuthHeaders(request)),
                    Void.class);

            return order;
        } else return null;
    }
}

